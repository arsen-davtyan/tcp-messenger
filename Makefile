CC= gcc
CFLAGS= -g

LIBS =

all:  client server

client: client.c client.h
	$(CC) $(CFLAGS) -o client -Wall client.c $(LIBS)

server: server.c server.h
	$(CC) $(CFLAGS) -o server -Wall server.c $(LIBS)

clean:
	rm -f server client *.o



