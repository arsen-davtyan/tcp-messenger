# Tcp Messenger

Простой TCP мессенджер написанный на C. Был протестирован в домашней сети между двумя разными устройствами. Однако функционал можно посмотреть и запустив **client**-ов в нескольких терминалах и сервер на **localhost** (_127.0.0.1_).

### Workflow

- Скомпилировать исходники с помощью **Makefile**.

    `make`


- Запустить **server**  в терминале коммандой

    `./server`

    или 

    `./server {port}`'

    В первом случае порт выберется случайно и выведется в терминал



-  Запустить **client**-ов  в других терминалах / на других устройствах коммандой

    `./client {handle} {host-ip} {port}`

    где _handle_ - имя которое будет видно в мессенджере.

- Написать **%H** со стороны клиента, чтобы посмотреть доступные комманды.
